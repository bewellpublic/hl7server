package com.example.demo.hl7

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class HL7FhirController {
    @GetMapping("/api")
    fun onServerStatusCheck() {
        println("Server is up, 200")
    }

    @PostMapping("/api/send")
    fun onHL7FhirDataReceived(@RequestBody data: HL7Bundle) {
        println(data.toString())
        ResponseEntity.ok(data)
    }
}


