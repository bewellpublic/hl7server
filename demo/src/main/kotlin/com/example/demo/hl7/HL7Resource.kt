package com.example.demo.hl7

data class HL7Resource(
    var resourceType: String,
    var id: String,
    var status: String,
    var code: HL7Code,
    var subject: HL7Subject,
    var issued: String,
    var valueQuantity: HL7Value
)
