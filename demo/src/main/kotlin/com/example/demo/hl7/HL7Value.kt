package com.example.demo.hl7

import javax.validation.constraints.Positive

data class HL7Value(
    @Positive(message = "Measurement value must be positive.")
    var value: Double,
    var unit: String,
    var system: String,
    var code: String
)
