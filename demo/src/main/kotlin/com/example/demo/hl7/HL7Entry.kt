package com.example.demo.hl7

import javax.validation.constraints.NotNull

data class HL7Entry(
    @NotNull(message = "Body must contain a resource")
    var resource: HL7Resource?
)
