package com.example.demo.hl7

data class HL7Code(
    var coding: List<HL7Coding>
)
