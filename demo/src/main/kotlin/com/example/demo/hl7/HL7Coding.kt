package com.example.demo.hl7

data class HL7Coding(
    var system: String,
    var code: String,
    var display: String
)
