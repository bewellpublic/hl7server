package com.example.demo.hl7

import javax.validation.constraints.NotNull
import javax.validation.constraints.Past

data class HL7Bundle(
    @NotNull(message = "Bundle ID cannot be null.")
    var id: String,
    var resourceType: String,
    var type: String,
    @Past(message = "Measurement timestamp can't be in future.")
    var timestamp: String,
    @NotNull(message = "Measurement entry cannot be null.")
    var entry: List<HL7Entry>
)
