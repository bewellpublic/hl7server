package com.example.demo.hl7

import javax.validation.constraints.NotNull

data class HL7Subject(
    @NotNull(message = "Identification of the patient must not be null")
    var reference: String
)
