# HL7 Server
### Sample project to facilitate the integration of the Visiocheck in your environment

To facilitate the integration of the Visiocheck into your environment, a server has been developed to receive HL7 data from the Visiocheck. By following the instructions below, you can observe the exchange mechanism between the Visiocheck and the server to facilitate the development of your own. This section explains how to easily setup a local HL7 FHIR server, and how to configure Visiocheck device in order to communicate with this server.

**Disclaimer: THE TEST SERVER SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.**



## Server setup
To get started with HL7 FHIR Server, you will have to download an IDE (we strongly recommend [IntelliJ IDE community edition](https://www.jetbrains.com/fr-fr/idea/download/download-thanks.html?platform=windows&code=IIC)) in which we can run and modify the sample provided here [HL7Server](https://gitlab.com/bewellpublic/hl7server/-/tree/develop).

:warning: **Run** **DemoApplication.kt as main class to start the server on localhost, port 8888. To be mentioned that the whole code for server part is written in Kotlin.**

The project is quite simple, containing two endpoints located in HL7FhirController.kt and looks pretty much like in the following snippet:

```sh
@RestController
class HL7FhirController {
    @GetMapping("/api")
    fun onServerStatusCheck () {
        println("Server is up, 200")
    }

    @PostMapping("/api/send")
    fun onHL7FhirDataReceived(@RequestBody data: HL7Bundle) {
        println(data.toString())
        ResponseEntity.ok(data)
    }
}
```
Please find below some explanation related to the snippet:
- first endpoint "/api" will be used in order to check the connection to the server (like a ping to our HL7 FHIR server). If connection is ok, server should print “Server is up, 200”
- second endpoint "/api/send" will be our main endpoint which will receive the measurements from the Visiocheck device. As an example, a thermometer measurement received and printed without parsing looks like
```sh
HL7Bundle(id=bundle_id_to_be_replaced_by_backend, resourceType=Bundle, type=Collection, timestamp=2021-12-21T14:54:42.140+0100, entry=[HL7Entry(resource=HL7Resource(resourceType=Observation, id=id, status=final, code=HL7Code(coding=[HL7Coding(system=http://loinc.org, code=8310-5, display=Body temperature)]), subject=HL7Subject(reference=FIRSTNAME,LASTNAME), issued=2021-12-21T14:54:38.411+0100, valueQuantity=HL7Value(value=98.96, unit=°F, system=http://snomed.info/sct, code=258710007)))])
```
:warning: **Important to be mentioned here, the endpoint created to check connection with server must be part of the second one.**
> E.g. @GetMapping("myurl/api/v1") and @PostMapping("myurl/api/v1/something").
 
**Visiocheck will automatically check the server connection status based on the URL typed by removing the last characters until the last “/” of this URL.**
> E.g. if URL is “myurl/api/v1/something”, the check will be done using “myurl/api/v1” endpoint

The sample also contains an API key validation class, named ApiKeyRequestFilter.kt which looks like this:
```sh
@Component
public class ApiKeyRequestFilter extends GenericFilterBean {

    private static final Logger LOG = LoggerFactory.getLogger(ApiKeyRequestFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        String path = req.getRequestURI();

        if (!path.startsWith("/api")) {
            chain.doFilter(request, response);
            return;
        }

        String key = req.getHeader("ApiKey") == null ? "" : req.getHeader("ApiKey");
        LOG.info("Trying key: " + key);

        if (key != null && key.equals("a2001f6c-6ac9-4274-8950-27a5383437e5")) {
            chain.doFilter(request, response);
        } else {
            HttpServletResponse resp = (HttpServletResponse) response;
            String error = "Invalid API KEY";

            resp.reset();
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.setContentLength(error.length());
            response.getWriter().write(error);
        }
    }
}
```
This class makes sure that only requests that contains /api will be verified for a valid API KEY. For 
the sake of testing, we have hardcoded an API KEY there (we will need to use it on Visiocheck device also in the header of our requests).
Without this api key **a2001f6c-6ac9-4274-8950-27a5383437** connection to server won’t work and you can’t configure the device properly, therefore no measurement will be sent.

:warning: **This sample is only an example, it must be written according to your own ApiKeyRequestFilter or logic you want.**



## Visiocheck configuration
Once the server is up and running, you need to configure the Visiocheck device. What does this mean? It is simple, just open the application, login and navigate to Side Menu (top right corner of main screen) → Settings → HL7 FHIR. To test, in the 4th screen below, enter the URL of the server **http://myurl:8888/api/send**, the API Key **a2001f6c-6ac9-4274-8950-27a5383437**, and select one or more Identifiers. If configured properly, the completed screen looks like the 4th one:

![HL7_ VCK_SETUP.png](./HL7_ VCK_SETUP.png)

Once you validate (and if everything is ok) your connection status will change to success.

:warning: At first, you might encounter an issue even if you configured Visiocheck properly connection is still not ok, then make sure Visiocheck device is connected (USB) to the machine hosting the local server, open a terminal and run the following command 
```sh 
adb reverse tcp:8888 tcp:8888
```

After following these steps (activation of the HL7 switch, screen 3 above, and configuration of the communication, screen 4 above), navigate to the screens of the different measurements to send them to the server. The measurements are sent automatically by pressing the "Save measurement" button from the thermometer, BPM, oximeter, scale or glucometer on Visiocheck screens.


